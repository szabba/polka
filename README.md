# `polka`

Functional programming for Python.

# Install

```bash
$ pip install polka
```

# Run tests

We only use `setup.py test` to install the test dependencies.

```bash
$ python setup.py test
```

We'll let pytest deal with actually running tests.

```bash
$ py.test
```

# Test coverage

```bash
$ pip install pytest-cov
$ py.test -cov=polka