#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

from setuptools import setup, find_packages


setup(

    name='polka',
    description='Functional programming library for Python',
    version='0.1',

    url='http://bitbucket.org/szabba/polka',
    license='MPL-2.0',

    classifiers=[
        'Development Status :: 1 - Planning',

        'Intended Audience :: Developers',
        'Topic :: Software Development',

        'License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)',
        'Programming Language :: Python :: 3',
    ],
    keywords=['functional programming', 'fp', 'immutable'],

    packages=find_packages(exclude=['test', 'test.*']),

    author='Karol Marcjan',
    author_email='karol.marcjan@gmail.com',

    install_requires=['hypothesis>=1.14,<2'],

    tests_require=[
        'pytest >=2.8, <3',
        'hypothesis >=2, <3'
    ],
)
