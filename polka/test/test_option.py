#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

import unittest
import doctest

from hypothesis.strategies import integers, just, one_of

from polka import option
from polka.option import Present, Missing
from polka.cats.functor import functor_laws
from polka.meta.hypothesis.functions import integer_functions


@functor_laws
class OptionObeysFunctorLaws(unittest.TestCase):

    class generators:
        functor_value = one_of(integers().map(Present), just(Missing()))
        inner_function = integer_functions()
        outer_function = integer_functions()
