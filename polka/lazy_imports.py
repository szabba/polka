#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

import importlib


__all__ = ['LazyImport']


class LazyImport:
    """LazyImport([module=None]) creates an object that can be used to import
    modules upon first access of their members - including submodules.

    >>> math = LazyImport('math')
    >>> math.cos(0)
    1.0

    >>> from http.client import HTTPConnection
    >>> HTTPConnection == LazyImport('http').client.HTTPConnection
    True
    """

    def __init__(self, module_name):
        self.__module_name = module_name
        self.__module = None

    def __get_module(self):
        if self.__module is None:
            self.__module = importlib.import_module(self.__module_name)
        return self.__module

    def __getattr__(self, attr):
        return getattr(self.__get_module(), attr)
