#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

import abc
import functools

from polka.lazy_imports import LazyImport
from polka.adt import algebraic_data_type
from polka.cats.functor import Functor


__all__ = ['Result', 'Success', 'Failure']


option = LazyImport('polka.option')


@algebraic_data_type
class Result(Functor, abc.ABC):
    """A Result represents either a value or an error condition."""

    variants = [
        ('Success', 1),
        ('Failure', 1),
    ]

    @staticmethod
    def do(f):
        """Result.do(f) runs f and returns an Result. When f raises an
        exception, the result is a Failure, otherwise it's a Success.

        >>> Result.do(lambda: 1 / 0)
        Failure(ZeroDivisionError('division by zero',))
        >>> Result.do(lambda: 1 / 2)
        Success(0.5)

        It's an interesting idiom to use Result.do as a decorator. The examples
        above don't do it for the sake of breviety, but it can be useful for
        capturing the output of a longer block of code.

        >>> @Result.do
        ... def x():
        ...     # Insert a lot of imaginary code here.
        ...     return 3
        >>> x.map(lambda x: x + 5)
        Success(8)
        """

        return Result.capture(f)()

    @staticmethod
    def capture(f):
        """Result.capture(f) creates a version of f that returns a Result.
        Instead of raising an exception the function will return a Failure
        wrapping it. A succesfully returned value will (as you migt've
        guessed) be wrapped in a Success.

        >>> one_over = Result.capture(lambda x: 1 / x)
        >>> one_over(2)
        Success(0.5)
        >>> one_over(0)
        Failure(ZeroDivisionError('division by zero',))

        R.handle_result(on_success, on_failure) calls the appropriate
        callback depending on whether the process that created the Result was
        successful or not.

        >>> Success(3).handle_result(
        ...     lambda x: x,
        ...     lambda _: None)
        3

        >>> Failure('some error').handle_result(
        ...     lambda _: None,
        ...     lambda x: x)
        'some error'
        """

        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            try:
                return Success(f(*args, **kwargs))
            except Exception as err:
                return Failure(err)

        return wrapper

    def get(self, default):
        """R.get(default) tries to retrieve the inner value, falling back on
        the specified default if necessary.

        >>> Success(3).get(0)
        3

        >>> Failure('some error').get(0)
        0
        """

        return self.handle_result(
            lambda inner: inner,
            lambda _: default)

    def get_lazy(self, default_func):
        """R.get_lazy(default_func) tries to retrieve the inner value, falling back
        on the result of the specified function if necessary.

        >>> def gimme_zero():
        ...     print('getting zero')
        ...     return 0
        ...

        >>> Success(3).get_lazy(gimme_zero)
        3
        >>> Failure('some error').get_lazy(gimme_zero)
        getting zero
        0
        """

        return self.handle_result(
            lambda inner: inner,
            lambda _: default_func())

    def map(self, f):
        """R.map(f) transforms the inner value (if any) using f.

        >>> Success(3).map(lambda x: x - 5)
        Success(-2)

        >>> Failure('some error').map(lambda x: x - 5)
        Failure('some error')
        """

        return self.handle_result(
            lambda inner_val: Success(f(inner_val)),
            lambda _: self)

    def join(self):
        """R.join() flattens one level of Result nesting.

        >>> Success(Success(3)).join()
        Success(3)

        >>> Success(Failure('some error')).join()
        Failure('some error')

        >>> Failure('some error').join()
        Failure('some error')

        >>> Success(3).join()
        Traceback (most recent call last):
            ...
        TypeError: inner value 3 is not a Result
        """

        def return_if_result(inner_res):
            if not isinstance(inner_res, Result):
                raise TypeError(
                    'inner value {} is not a Result'.format(repr(inner_res)))
            return inner_res

        return self.handle_result(
            return_if_result,
            lambda _: self)

    def flat_map(self, f):
        """O.flat_map(f) maps over a value with an Option-returning function
        flattening the result.

        >>> def safe_divide(x, y):
        ...     if y == 0:
        ...         return Failure('division by zero')
        ...     else:
        ...         return Success(x / y)
        ...

        >>> Success(3).flat_map(lambda x: safe_divide(x, 0))
        Failure('division by zero')

        >>> Success(3).flat_map(lambda x: safe_divide(x, 4))
        Success(0.75)

        >>> Failure('some error').flat_map(lambda x: safe_divide(x, 4))
        Failure('some error')

        We can't forbid programs where the types don't make sense from being
        constructed. When `flat_map` is being called on a succesful result we
        raise a `TypeError` if the function being called returns a non-result
        value.

        >>> Success(3).flat_map(lambda x: x - 5)
        Traceback (most recent call last):
            ...
        TypeError: inner value -2 is not a Result

        Note: this doesn't work for failed computations, as the function never
        gets called.

        >>> Failure('some error').flat_map(lambda x: x - 5)
        Failure('some error')
        """

        return self.map(f).join()

    def recover(self, f):
        """R.recover(f) uses f to handle the error, if any. f must be a
        result-returning function.

        >>> Success(3).recover(lambda _: Success(0))
        Success(3)

        >>> Failure('division by zero').recover(lambda _: Success(0))
        Success(0)

        >>> Failure('division by zero').recover(
        ...     lambda _: Failure('some error'))
        Failure('some error')
        """

        return self.handle_result(lambda _: self, f)

    def map_safe(self, f):
        """R.map_safe(f) behaves like map, except it catches exceptions and
        transforms them into failures.

        >>> Success(3).map(lambda x: x / 0)
        Traceback (most recent call last):
            ...
        ZeroDivisionError: division by zero

        >>> Success(3).map_safe(lambda x: x / 0)
        Failure(ZeroDivisionError('division by zero',))

        It makes no difference when mapping over an already failed computation
        of course.

        >>> Failure('some error').map_safe(lambda x: x / 0)
        Failure('some error')
        """

        try:
            return self.map(f)
        except Exception as err:
            return Failure(err)

    def flat_map_safe(self, f):
        """R.flat_map_safe(f) behaves like flat_map except it catches
        exceptions and transform them into failures.

        >>> Success(3).map(lambda x: x / 0)
        Traceback (most recent call last):
            ...
        ZeroDivisionError: division by zero

        >>> Success(3).flat_map_safe(lambda x: Success(x / 0))
        Failure(ZeroDivisionError('division by zero',))

        Note: the behaviour when the argument function does not return a
        Result is left unspecified.
        """

        try:
            return self.flat_map(f)
        except Exception as err:
            return Failure(err)

    def to_option(self):
        """R.to_option() converts a Result to an Option value, losing any
        present error information in the process.

        >>> Success(3).to_option()
        Present(3)

        >>> Failure('some error').to_option()
        Missing()
        """

        return self.handle_result(
            option.Present,
            lambda _: option.Missing())


Success, Failure = Result.Success, Result.Failure
