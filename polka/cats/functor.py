#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

import abc
from hypothesis import given


__all__ = ['Functor']


class Functor(abc.ABC):

    @abc.abstractmethod
    def map(self, f):
        """F.map(f) maps f over values contained in the Functor (if any)."""


def functor_laws(cls):
    """A decorator that turns unittest test cases providing a couple generators
    into tests of functor laws.

    The functor identity law says that after mapping with the identity function
    we should recover the original functor:

        c.map(lambda x: x) == c

    The functor composition law says that mapping with one two functions in
    sequence must give the same result as mapping with their composition:

        c.map(f).map(g) == c.map(lambda x: g(f(x)))

    The class being decorated (lets refer to it as cls) needs to provide three
    generators for the hypothesis package:

    * cls.generators.functor_value
    * cls.generators.inner_function
    * cls.generators.outer_function
    """

    @given(cls.generators.functor_value)
    def test_functor_identity_law(self, functor_value):

        self.assertEqual(functor_value.map(lambda x: x),
                         functor_value)

    @given(cls.generators.functor_value,
           cls.generators.inner_function,
           cls.generators.outer_function)
    def test_functor_composition_law(self, functor_value, f, g):

        def composed(x):
            return g(f(x))

        self.assertEqual(functor_value.map(f).map(g),
                         functor_value.map(composed))

    cls.test_functor_identity_law = test_functor_identity_law

    cls.test_functor_composition_law = test_functor_composition_law

    return cls
