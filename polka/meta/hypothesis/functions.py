#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

from hypothesis.strategies import integers, one_of, just


__all__ = ['integer_functions']


def integer_functions():
    return (
        integers().filter(
            lambda x: x != 0)
        .flatmap(
            lambda n: one_of(
                just(lambda x: x + n),
                just(lambda x: x - n),
                just(lambda x: x % n),
                just(lambda x: x * n),
                just(lambda x: x // n))))
