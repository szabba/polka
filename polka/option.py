#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

import abc
import functools

from polka.lazy_imports import LazyImport
from polka.adt import algebraic_data_type
from polka.cats.functor import Functor


__all__ = ['Option', 'Present', 'Missing']


result = LazyImport('polka.result')


@algebraic_data_type
class Option(Functor, abc.ABC):
    """An Option represents a value that might be present or missing."""

    variants = [
        ('Present', 1),
        ('Missing', 0),
    ]

    @staticmethod
    def do(f):
        """Option.do(f) runs f and returns an Option. When f raises an
        exception or returns None, the result is Missing(), otherwise it's
        whatever f has returned wrapped in a Present.

        >>> Option.do(lambda: 1 / 0)
        Missing()
        >>> Option.do(lambda: 1 / 2)
        Present(0.5)
        >>> Option.do(lambda: {}.get('x'))
        Missing()

        It's an interesting idiom to use Option.do as a decorator. The examples
        above don't do it for the sake of breviety, but it can be useful for
        capturing the output of a longer block of code.

        >>> @Option.do
        ... def x():
        ...     # Insert a lot of imaginary code here.
        ...     return 3
        >>> x.map(lambda x: x + 5)
        Present(8)
        """

        return Option.capture(f)()

    @staticmethod
    def capture(f):
        """Option.capture(f) creates a version of f that returns an Option.
        When the funtion raises an exception or returns None, the transformed
        version returns Missing(), otherwise the result is wrapped in a
        Present.

        >>> one_over = Option.capture(lambda x: 1 / x)
        >>> one_over(2)
        Present(0.5)
        >>> one_over(0)
        Missing()

        >>> phones = {'Joe': 123, 'Kate': 980}
        >>> get_phone = Option.capture(phones.get)
        >>> get_phone('Kate')
        Present(980)
        >>> get_phone('Mike')
        Missing()

        O.handle_option(when_present, when_mising) calls the appropriate
        callback depending on whether the value is present or not.

        >>> Present(3).handle_option(
        ...     lambda x: x,
        ...     lambda: 'oops')
        3

        >>> Missing().handle_option(
        ...     lambda x: x,
        ...     lambda: 'oops')
        'oops'
        """

        @functools.wraps(f)
        def wrapper(*args, **kwargs):
            try:
                res = f(*args, **kwargs)
                return Missing() if res is None else Present(res)
            except:
                return Missing()

        return wrapper

    def get(self, default):
        """O.get(default) tries to retrieve the inner value, falling back on
        the specified default if necessary.

        >>> Present(3).get(0)
        3

        >>> Missing().get(0)
        0
        """

        return self.handle_option(
            lambda inner_val: inner_val,
            lambda: default)

    def get_lazy(self, default_func):
        """O.get_lazy(default_func) tries to retrieve the inner value, falling
        back on the result of the specified function if necessary.

        >>> def gimme_zero():
        ...     print('getting zero')
        ...     return 0
        ...

        >>> Present(3).get_lazy(gimme_zero)
        3
        >>> Missing().get_lazy(gimme_zero)
        getting zero
        0
        """

        return self.handle_option(
            lambda inner_value: inner_value,
            default_func)

    def map(self, f):
        """O.map(f) transforms the inner value, if any.

        >>> Present(3).map(lambda x: x - 5)
        Present(-2)

        >>> Missing().map(lambda x: x - 5)
        Missing()
        """

        return self.handle_option(
            lambda val: Present(f(val)),
            lambda: self)

    def join(self):
        """O.join() flattens one level of option nesting.

        >>> Present(Present(3)).join()
        Present(3)

        >>> Present(Missing()).join()
        Missing()

        >>> Missing().join()
        Missing()

        >>> Present(3).join()
        Traceback (most recent call last):
            ...
        TypeError: inner value 3 is not an Option
        """

        def return_if_option(inner_opt):
            if not isinstance(inner_opt, Option):
                raise TypeError(
                    'inner value {} is not an Option'.format(inner_opt))
            return inner_opt

        return self.handle_option(return_if_option, lambda: self)

    def flat_map(self, f):
        """O.flat_map(f) maps over a value with an Option-returning function
        flattening the result.

        >>> def safe_divide(x, y):
        ...     if y == 0:
        ...         return Missing()
        ...     else:
        ...         return Present(x / y)
        ...

        >>> Present(3).flat_map(lambda x: safe_divide(x, 0))
        Missing()

        >>> Present(3).flat_map(lambda x: safe_divide(x, 4))
        Present(0.75)

        >>> Missing().flat_map(lambda x: safe_divide(x, 4))
        Missing()

        We can't forbid programs where the types don't make sense from being
        constructed. When `flat_map` is being called on a present value we
        raise a `TypeError` if the function being called returns a non-Option
        value.

        >>> Present(3).flat_map(lambda x: x - 5)
        Traceback (most recent call last):
            ...
        TypeError: inner value -2 is not an Option

        Note: this doesn't work on missing values, as the function never gets
        called.

        >>> Missing().flat_map(lambda x: x - 5)
        Missing()
        """

        return self.map(f).join()

    def map_safe(self, f):
        """O.map_safe(f) behaves like map, except it catches exceptions and
        transforms them to missing values.

        >>> Present(3).map(lambda x: x / 0)
        Traceback (most recent call last):
            ...
        ZeroDivisionError: division by zero

        >>> Present(3).map_safe(lambda x: x / 0)
        Missing()

        It makes no difference when mapping over a missing value of course.

        >>> Missing().map_safe(lambda x: x / 0)
        Missing()
        """

        try:
            return self.map(f)
        except:
            return Missing()

    def flat_map_safe(self, f):
        """O.flat_map_safe(f) behaves like flat_map except it catches
        exceptions and transform them into missing values.

        >>> Present(3).map(lambda x: x / 0)
        Traceback (most recent call last):
            ...
        ZeroDivisionError: division by zero

        >>> Present(3).flat_map_safe(lambda x: Present(x / 0))
        Missing()

        Note: the behaviour when the argument function does not return an
        Option is left unspecified.
        """

        try:
            return self.flat_map(f)
        except:
            return Missing()

    def to_result(self, err):
        """O.to_result(err) converts an Option to a Result. When there's no
        value, it produces a failure wrapping the specified error.

        >>> Present(3).to_result(None)
        Success(3)

        >>> Missing().to_result(None)
        Failure(None)
        """
        return self.handle_option(
            result.Success,
            lambda: result.Failure(err))


Missing, Present = Option.Missing, Option.Present
