#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.

import re
import abc


__all__ = ['algebraic_data_type']


def algebraic_data_type(cls):
    """algebraic_data_type is a decorator for defining algebraic data types.

    The class being decorated needs to be an abstract base class.

    >>> import abc

    The class's variants field has to specify a list of names and argument
    counts for the datatype variants.

    >>> @algebraic_data_type
    ... class NonEmptyList(abc.ABC):
    ...     variants = [
    ...         ('Cons', 2),
    ...         ('Singleton', 1),
    ...     ]
    ...

    Each declared variant will become a subclass of the datatype, accessible as
    a field of the base class.

    >>> isinstance(NonEmptyList.Cons, type)
    True

    >>> isinstance(NonEmptyList.Singleton, type)
    True

    The subclasses print out reasonably.

    >>> Cons, Singleton = NonEmptyList.Cons, NonEmptyList.Singleton

    >>> Cons(1, Cons(1, Cons(2, Singleton(3))))
    Cons(1, Cons(1, Cons(2, Singleton(3))))

    Equality and hashing work as expected.

    >>> hash(Singleton(1)) == hash(Singleton(1))
    True

    >>> Singleton(1) == Singleton(1)
    True

    >>> Singleton(2) == Singleton(1)
    False

    >>> Cons(1, Singleton(2)) == Cons(1, Singleton(2))
    True

    Each class gets a variant handler method defined. The variant handler
    emulates pattern matching by taking one function to handle each possible
    variant of the ADT. The implementation for each variant passes the arguments
    that it received in __init__ to the function passed at the position of the
    correspodning variant. The order of functions is the same as the order of
    variants specified in the class definition.

    >>> @algebraic_data_type
    ... class Bool(abc.ABC):
    ...     variants = [('Bullshit', 0), ('Truth', 0)]
    ...
    >>> Bool.Bullshit().handle_bool(lambda: False, lambda: True)
    False

    >>> @algebraic_data_type
    ... class Tree(abc.ABC):
    ...     variants = [('Leaf', 0), ('Branch', 3)]
    ...     def has_branches(self):
    ...         return self.handle_tree(
    ...             lambda: False,
    ...             lambda left, value, right: True)
    ...
    >>> Tree.Leaf().has_branches()
    False
    >>> Tree.Branch(Tree.Leaf(), 4, Tree.Leaf()).has_branches()
    True
    """

    attach_variant_handler(cls)

    for nth, variant_spec in enumerate(cls.variants):
        attach_variant(cls, nth, variant_spec)

    return cls


def attach_variant_handler(cls):

    @abc.abstractmethod
    def variant_handler(self, *callbacks):
        if not len(callbacks) == len(cls.variants):
            raise TypeError('{} callbacks needed, {} specified')

    variant_handler.__name__ = variant_handler_name(cls.__name__)

    cls.variant_handler = variant_handler


def attach_variant(base_cls, nth, variant_spec):
    name, _ = variant_spec
    cls = variant(base_cls, nth, variant_spec)
    setattr(base_cls, name, cls)


def variant_handler_name(cls_name):
    """variant_handler_name(cls_name) -> string

    Returns the variant handler name for the glass with the given name.

    >>> variant_handler_name("Point")
    'handle_point'

    >>> variant_handler_name("HTTPRequestHandler")
    'handle_http_request_handler'

    >>> variant_handler_name('XMLRPCRequest')
    'handle_xmlrpc_request'
    """
    cls_name_parts = [
        reverse_part[::-1]
        for reverse_part
        in re.findall('[a-z]+[A-Z]|[A-Z][A-Z]+', cls_name[::-1])
    ][::-1]

    variant_handler_name_parts = (
        ['handle'] +
        [cls_name_part.lower() for cls_name_part in cls_name_parts])

    return '_'.join(variant_handler_name_parts)


def variant(base_cls, nth, variant_spec):
    name, arg_count = variant_spec

    args_field_name = '_{}_args'.format(name)

    def __init__(self, *args):
        if not len(args) == arg_count:
            raise TypeError(
                'you need {} args to create a {}'.format(arg_count, name))
        setattr(self, args_field_name, args)

    def __repr__(self):
        args = getattr(self, args_field_name)
        return '{}({})'.format(name, ', '.join(map(repr, args)))

    def __hash__(self):
        return hash(getattr(self, args_field_name))

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        args = getattr(self, args_field_name)
        others_args = getattr(other, args_field_name)
        return args == others_args

    cls = type(
        name,
        (base_cls, ),
        {
            '__init__': __init__, '__repr__': __repr__,
            '__hash__': __hash__, '__eq__': __eq__,
        })

    def variant_handler(self, *callbacks):
        super(cls, self).variant_handler(*callbacks)
        args = getattr(self, args_field_name)
        return callbacks[nth](*args)

    variant_handler.__name__ = variant_handler_name(base_cls.__name__)

    setattr(cls, variant_handler.__name__, variant_handler)

    return cls
